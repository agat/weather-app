// @flow

import * as React from 'react';
import {
    Link,
} from 'react-router-dom';
import {
    getAllCountries,
} from 'country-state-city';

import AppBar from '../../components/AppBar';
import ListItem from '../../components/ListItem';


const CountriesPage = () => (
    <>
        <AppBar
            title="Countries"
            backTo="/"
        />
        {getAllCountries().map(({
            id,
            name,
        }) => (
            <ListItem
                as={Link}
                to={`/country/${id}`}
                key={id}
            >
                {name}
            </ListItem>
        ))}
    </>
);

export default CountriesPage;
