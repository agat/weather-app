// @flow

import * as React from 'react';
import {
    getCountryById,
    getStatesOfCountry,
    getCitiesOfState,
} from 'country-state-city';

import {
    addCity,
    removeCity,
} from '../../reducer/actions';
import {
    getAndUpdateCityWeather,
    getAndUpdateCityCoordinates,
} from '../../utils/reducer';

import AppBar from '../../components/AppBar';
import Subtitle from '../../components/Subtitle';
import Box from '../../components/Box';
import CityItem from '../../components/CityItem';

import {
    type PageT,
} from '../../types';


const CountryPage = ({
    match,
    state,
    dispatch,
}: PageT) => {
    const country = getCountryById(match.params.id);

    return (
        <>
            <AppBar
                title={country.name}
                backTo="/countries/"
            />
            {getStatesOfCountry(country.id).map(countryState => (
                <div
                    key={`${country.id}-${countryState.id}`}
                >
                    <Box>
                        <Subtitle>
                            {countryState.name}
                        </Subtitle>
                    </Box>
                    {getCitiesOfState(countryState.id).map(city => (
                        <CityItem
                            key={city.id}
                            city={city}
                            cities={state.cities}
                            onAdd={() => {
                                dispatch(addCity(city));
                                getAndUpdateCityWeather(city, dispatch);
                                getAndUpdateCityCoordinates(city, dispatch);
                            }}
                            onRemove={id => dispatch(removeCity(id))}
                        />
                    ))}
                </div>
            ))}
        </>
    );
};

export default CountryPage;
