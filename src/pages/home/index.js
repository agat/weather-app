// @flow

import * as React from 'react';
import {
    Link,
} from 'react-router-dom';
import SearchIcon from '@material-ui/icons/Search';
import CountriesIcon from '@material-ui/icons/Folder';

import {
    getAndUpdateCurrentLocation,
} from '../../utils/reducer';

import AppBar, {
    ActionIcon,
} from '../../components/AppBar';
import CityWeather from '../../components/CityWeather';
import CurrentLocationWeather from '../../components/CurrentLocationWeather';

import Subtitle from '../../components/Subtitle';
import Button from '../../components/Button';
import Box from '../../components/Box';

import {
    type PageT,
} from '../../types';


const HomePage = ({
    state,
    dispatch,
}: PageT) => (
    <>
        <AppBar
            title="Weather"
            actions={[
                <ActionIcon
                    key="search"
                    as={Link}
                    to="/search"
                >
                    <SearchIcon />
                </ActionIcon>,
                <ActionIcon
                    key="countries"
                    as={Link}
                    to="/countries"
                >
                    <CountriesIcon />
                </ActionIcon>,
            ]}
        />
        <Box>
            <Subtitle>
                Current location
            </Subtitle>
        </Box>
        <Box>
            {!!state.currentLocation && (
                <CurrentLocationWeather
                    location={state.currentLocation}
                    dispatch={dispatch}
                />
            )}
            {state.currentLocation === null && (
                <Button
                    onClick={() => getAndUpdateCurrentLocation(dispatch)}
                >
                    Detect current location
                </Button>
            )}
        </Box>
        {!!state.cities.length && (
            <>
                <Box>
                    <Subtitle>
                        Cities
                    </Subtitle>
                </Box>
                {state.cities.map(city => (
                    <Box
                        key={city.id}
                    >
                        <CityWeather
                            city={city}
                            dispatch={dispatch}
                        />
                    </Box>
                ))}
            </>
        )}
        {(!!state.currentLocation || !!state.cities.length) && (
            <Box>
                <Button
                    as={Link}
                    to="/map"
                >
                    Show on map
                </Button>
            </Box>
        )}
    </>
);

export default HomePage;
