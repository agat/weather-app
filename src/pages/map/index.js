// @flow

import * as React from 'react';
import styled from 'styled-components';
import {
    GoogleMap,
    LoadScript,
    OverlayView,
} from '@react-google-maps/api'

import AppBar from '../../components/AppBar';
import Weather from '../../components/Weather';

import {
    type StateT,
} from '../../reducer';

type PropsT = {
    state: StateT
};

const getPositionFromCoordinates = ({ lat, lon }) => ({
    lat: parseFloat(lat),
    lng: parseFloat(lon),
});

const CountryPage = ({
    match,
    state,
    dispatch,
}: PropsT) => {
    const {
        currentLocation,
        cities,
    } = state;
    const markers = [];
    let coordinates = {};

    if (currentLocation) {
        coordinates = getPositionFromCoordinates(currentLocation.coordinates);

        markers.push({
            key: 'current',
            position: coordinates,
            data: {
                city: currentLocation.city,
                temp: currentLocation.weather.temp,
            },
        });
    }

    cities.forEach(city => markers.push({
        key: city.id,
        position: getPositionFromCoordinates(city.coordinates),
        data: {
            city: city.name,
            temp: city.weather.temp,
        },
    }));

    return (
        <>
            <AppBar
                title="Map"
                backTo="/"
            />
            <LoadScript
                id="script-loader"
                googleMapsApiKey={process.env.REACT_APP_GOOGLE_MAPS_KEY}
            >
                <GoogleMap
                    id="weather-map"
                    mapContainerStyle={{
                        height: 'calc(100vh - 48px)',
                        width: '100%',
                    }}
                    zoom={7}
                    center={coordinates}
                >
                    {markers.map(({ key, position, data }) => (
                        <OverlayView
                            key={key}
                            position={position}
                            mapPaneName={OverlayView.OVERLAY_MOUSE_TARGET}
                        >
                            <OverlayViewBody>
                                <Weather
                                    city={data.city}
                                    temp={data.temp}
                                />
                            </OverlayViewBody>
                        </OverlayView>
                    ))}
                </GoogleMap>
            </LoadScript>
        </>
    );
};

const OverlayViewBody = styled.div`
    background: white;
    padding: 16px;
`;

export default CountryPage;
