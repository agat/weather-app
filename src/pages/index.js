// @flow

import * as React from 'react';
import {
    HashRouter as Router,
    Route,
    Switch,
} from 'react-router-dom';

import reducer, {
    type StateT,
    initialState,
    restoreState,
    saveState,
} from '../reducer';

import {
    getAndUpdateCurrentLocation,
} from '../utils/reducer';

import HomePage from './home';
import SearchPage from './search';
import CountriesPage from './countries';
import CountryPage from './country';
import MapPage from './map';

const restoredState = restoreState(initialState);

const App = () => {
    const [state, dispatch] = React.useReducer<StateT>(reducer, restoredState);
    const render = Component => props => (
        <Component
            {...props}
            state={state}
            dispatch={dispatch}
        />
    );

    React.useEffect(() => saveState(state), [state]);
    React.useEffect(() => {
        const {
            currentLocation,
        } = state;

        if (currentLocation === null) {
            getAndUpdateCurrentLocation(dispatch);
        }
    }, []);

    return (
        <Router>
            <Switch>
                <Route
                    path="/"
                    render={render(HomePage)}
                    exact
                />
                <Route
                    path="/countries"
                    render={render(CountriesPage)}
                />
                <Route
                    path="/search"
                    render={render(SearchPage)}
                />
                <Route
                    path="/country/:id"
                    render={render(CountryPage)}
                    exact
                />
                <Route
                    path="/map"
                    render={render(MapPage)}
                />
            </Switch>
        </Router>
    );
};

export default App;
