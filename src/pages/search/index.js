// @flow

import * as React from 'react';

import {
    addCity,
    removeCity,
} from '../../reducer/actions';
import {
    getAndUpdateCityWeather,
    getAndUpdateCityCoordinates,
} from '../../utils/reducer';

import AppBar from '../../components/AppBar';
import CitySearch from '../../components/CitySearch';

import {
    type PageT,
} from '../../types';


const SearchPage = ({
    state,
    dispatch,
}: PageT) => (
    <>
        <AppBar
            title="Search City"
            backTo="/"
        />
        <CitySearch
            cities={state.cities}
            onAdd={(city) => {
                dispatch(addCity(city));
                getAndUpdateCityWeather(city, dispatch);
                getAndUpdateCityCoordinates(city, dispatch);
            }}
            onRemove={id => dispatch(removeCity(id))}
        />
    </>
);

export default SearchPage;
