// @flow

import * as React from 'react';
import styled from 'styled-components';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import {
    Link,
} from 'react-router-dom';


type PropsT = {
    title: string,
    backTo: string,
    actions?: [],
};

const AppBar = ({
    title,
    backTo,
    actions,
}: PropsT) => (
    <Bar>
        {!!backTo && (
            <Icon
                to={backTo}
            >
                <ArrowBackIcon />
            </Icon>
        )}
        <Title>
            {title}
        </Title>
        {actions && actions.map(action => action)}
    </Bar>
);

AppBar.defaultProps = {
    actions: [],
};

const Bar = styled.div`
    position: sticky;
    top: 0;
    left: 0;
    right: 0;
    z-index: 100;
    background: black;
    height: 48px;
    color: white;
    display: flex;
    align-items: center;
    padding: 0 16px;
`;

const Icon = styled(Link)`
    color: inherit;
    text-decoration: none;
    display: flex;
    align-items: center;
    margin-right: 24px;
    width: 24px;
    height: 24px;
    overflow: hidden;
`;

export const ActionIcon = styled.div`
    color: inherit;
    text-decoration: none;
    display: flex;
    align-items: center;
    margin-left: 16px;
`;

const Title = styled.div`
    font-weight: 500;
    font-size: 20px;
    margin-right: 24px;
    flex: 1;
`;

export default AppBar;
