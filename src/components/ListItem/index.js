/* eslint-disable no-confusing-arrow */
// @flow

import styled from 'styled-components';


const ListItem = styled.div`
    color: inherit;
    background: ${props => props.selected ? '#ccc' : 'transparent'};
    text-decoration: none;
    display: block;
    position: relative;
    height: 48px;
    line-height: 48px;
    padding: 0 16px;
    font-size: 16px;
    text-decoration: none;
    overflow: hidden;
    user-select: none;
    box-sizing: border-box;
    text-overflow: ellipsis;
    white-space: nowrap;
    cursor: pointer;
`;

export default ListItem;
