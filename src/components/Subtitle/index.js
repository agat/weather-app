// @flow

import styled from 'styled-components';


const Subtitle = styled.div`
    display: block;
    font-size: 20px;
    font-weight: 500;
    user-select: none;
`;

export default Subtitle;
