// @flow

import styled from 'styled-components';


const Input = styled.input`
    display: block;
    width: 100%;
    outline: 0;
    height: 56px;
    line-height: 56px;
    font-size: 20px;
    border: 1px solid currentColor;
    border-radius: 4px;
    padding: 0 16px;
    border: 2px solid currentColor;
`;

export default Input;
