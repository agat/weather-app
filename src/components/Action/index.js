// @flow

import * as React from 'react';
import styled from 'styled-components';


type PropsT = {
    icon: any,
    children: any
};

const Action = ({
    icon,
    children,
    ...props
}: PropsT) => (
    <Wrapper
        {...props}
    >
        {!!icon && (
            <Icon>
                {icon}
            </Icon>
        )}
        {children}
    </Wrapper>
);

const Wrapper = styled.div`
    cursor: pointer;
    display: flex;
    align-items: center;
`;

const Icon = styled.div`
    margin-right: 4px;
    display: flex;
`;

export default Action;
