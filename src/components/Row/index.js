// @flow
/* eslint-disable no-confusing-arrow */

import * as React from 'react';
import styled from 'styled-components';

type PropsT = {
    narrow?: boolean
}

const Row: React.ComponentType<PropsT> = styled.div`
    margin-top: ${props => props.narrow ? '8px' : '16px'};

    &:first-child {
        margin: 0;
    }
`;

export default Row;
