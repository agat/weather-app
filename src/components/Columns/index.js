/* eslint-disable no-confusing-arrow */
// @flow

import styled from 'styled-components';

const Columns = styled.div`
    display: flex;
    align-items: ${props => props.align || 'center'};
`;

Columns.Col = styled.div`
    flex: ${props => props.narrow ? 'none' : '1'};
    padding-right: 16px;

    &:last-child {
        padding-right: 0;
    }
`;

export default Columns;
