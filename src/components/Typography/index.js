/* eslint-disable no-confusing-arrow */
// @flow

import styled from 'styled-components';

export const Headline4 = styled.h2`
    font-size: 34px;
    line-height: 40px;
    font-weight: 400;
`;

export const Headline5 = styled.h2`
    font-size: 24px;
    line-height: 32px;
    font-weight: 400;
`;

export const Headline6 = styled.h2`
    font-size: 20px;
    line-height: 32px;
    font-weight: 400;
`;

export const Text = styled.p`
    font-size: ${props => props.isSmall ? '12px' : '16px'};
    line-height: 24px;
`;

export default {
    Headline4,
    Headline5,
    Text,
};
