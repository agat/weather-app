// @flow

import * as React from 'react';

import {
    removeCurrentCity,
} from '../../reducer/actions';

import {
    getAndUpdateCurrentCityWeather,
} from '../../utils/reducer';

import Weather from '../Weather';

import {
    type CurrentLocationT,
} from '../../reducer';


type PropsT = {
    location: CurrentLocationT
};

const CurrentLocationWeather = ({
    location,
    dispatch,
}: PropsT) => {
    const [isUpdating, setUpdateStatus] = React.useState(false);
    const updateWeather = () => {
        if (isUpdating) {
            return;
        }

        setUpdateStatus(true);

        getAndUpdateCurrentCityWeather(location.countryCode, location.city, dispatch).then(() => setUpdateStatus(false));
    };
    const remove = () => dispatch(removeCurrentCity());

    return (
        <Weather
            city={location.city}
            country={location.country}
            temp={location.weather.temp}
            weather={location.weather.title}
            isUpdating={isUpdating}
            onUpdate={updateWeather}
            onRemove={remove}
        />
    );
};

export default React.memo<PropsT>(CurrentLocationWeather);
