// @flow

import * as React from 'react';

import {
    removeCity,
} from '../../reducer/actions';

import {
    getCountryByStateId,
} from '../../utils/country';
import {
    getAndUpdateCityWeather,
} from '../../utils/reducer';

import Weather from '../Weather';

import {
    type CitySavedT,
} from '../../reducer';


type PropsT = {
    city: CitySavedT
};

const CityWeather = ({
    city,
    dispatch,
}: PropsT) => {
    const [isUpdating, setUpdateStatus] = React.useState(false);
    const country = getCountryByStateId(city.state_id);
    const updateWeather = () => {
        if (isUpdating) {
            return;
        }

        setUpdateStatus(true);

        getAndUpdateCityWeather(city, dispatch).then(() => setUpdateStatus(false));
    };
    const remove = () => dispatch(removeCity(city.id));

    if (city.weather.temp === null) {
        //updateWeather();
    }

    return (
        <Weather
            city={city.name}
            country={country.name}
            temp={city.weather.temp}
            weather={city.weather.title}
            isUpdating={isUpdating}
            onUpdate={updateWeather}
            onRemove={remove}
        />
    );
};

export default React.memo<PropsT>(CityWeather);
