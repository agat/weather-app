// @flow

import * as React from 'react';
import styled from 'styled-components';


type PropsT = {
    children: number
};

const Temp = ({ children }: PropsT) => {
    let value = Math.round(children);
    let sign = '';

    if (value > 0) {
        sign = '+';
    }

    if (value < 0) {
        sign = '–';
    }

    value = Math.abs(value);

    return (
        <Temperature>
            {sign}
            {value}
            °
        </Temperature>
    );
};

const Temperature = styled.div`
    font-weight: bold;
    font-size: 24px;
`;

export default React.memo<PropsT>(Temp);
