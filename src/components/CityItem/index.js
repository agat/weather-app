// @flow

import * as React from 'react';

import ListItem from '../ListItem';

import {
    type CityT,
} from '../../types';

type PropsT = {
    city: CityT,
    cities: CityT[],
    onAdd: (city: CityT) => void,
    onRemove: (id: number) => void,
};

const CityItem = ({
    city,
    cities,
    onAdd,
    onRemove,
}: PropsT) => {
    const citiesIds = cities.map(({ id }) => id);
    const isSavedCity = id => citiesIds.includes(id);

    return (
        <ListItem
            key={city.id}
            onClick={() => {
                if (isSavedCity(city.id)) {
                    onRemove(city.id);
                } else {
                    onAdd(city);
                }
            }}
            selected={isSavedCity(city.id)}
        >
            {city.name}
        </ListItem>
    );
};

export default CityItem;
