// @flow

import * as React from 'react';
import SyncIcon from '@material-ui/icons/Sync';
import styled, {
    keyframes,
} from 'styled-components';


type PropsT = {
    showActivityIf: boolean,
    children: any
};

const Activity = ({
    showActivityIf,
    children,
}: PropsT) => {
    if (showActivityIf) {
        return (
            <ActivityIcon />
        );
    }

    return children;
};

const rotateKeyFrames = keyframes`
    from {
        transform: rotate(360deg);
    }

    to {
        transform: rotate(0deg);
    }
`;

const ActivityIcon = styled(SyncIcon)`
    animation: ${rotateKeyFrames} 2s linear infinite;
`;

export default Activity;
