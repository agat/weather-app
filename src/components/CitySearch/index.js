// @flow

import * as React from 'react';
import { useFormState } from 'react-use-form-state';

import allCities from 'country-state-city/lib/city.json';

import Subtitle from '../Subtitle';
import Box from '../Box';
import Input from '../Input';
import CityItem from '../CityItem';

import {
    type CityT,
} from '../../types';

type PropsT = {
    cities: CityT[],
    onAdd: (city: CityT) => void,
    onRemove: (id: number) => void,
};

const CitySearch = ({
    onAdd,
    onRemove,
    cities,
}: PropsT) => {
    const [result, setResult] = React.useState<CityT[]>([]);
    const [form, { text }] = useFormState({
        query: '',
    }, {
        onChange: (e, oldValues, values) => {
            const query: string = values.query.trim().toLowerCase();

            if (query) {
                const searchResult: CityT[] = allCities.filter(
                    ({ name }) => name.toLowerCase().includes(query),
                );

                setResult(searchResult.slice(0, 42));
            } else {
                setResult([]);
            }
        },
    });

    return (
        <>
            <Box>
                <Input
                    {...text('query')}
                    type="search"
                    placeholder="Search..."
                />
            </Box>
            {!!form.values.query && (
                <Box>
                    <Subtitle>Search result</Subtitle>
                </Box>
            )}
            {result.map(city => (
                <CityItem
                    key={city.id}
                    city={city}
                    cities={cities}
                    onAdd={onAdd}
                    onRemove={onRemove}
                />
            ))}
        </>
    );
};

export default CitySearch;
