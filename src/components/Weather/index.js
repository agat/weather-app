// @flow

import * as React from 'react';
import RefreshIcon from '@material-ui/icons/Refresh';
import RemoveIcon from '@material-ui/icons/Clear';

import {
    Headline4,
    Text,
} from '../Typography';
import Activity from '../Activity';
import Action from '../Action';
import Columns from '../Columns';
import Row from '../Row';
import Temp from '../Temp';


type PropsT = {
    city: string,
    country: string,
    temp: number,
    weather?: ?string,
    isUpdating: boolean,
    onUpdate?: ?() => void,
    onRemove?: ?() => void
};

const Weather = ({
    city,
    country,
    temp,
    weather,
    isUpdating,
    onUpdate,
    onRemove,
}: PropsT) => (
    <>
        <Row narrow>
            <Columns>
                <Columns.Col>
                    <Columns
                        align="flex-end"
                    >
                        <Columns.Col narrow>
                            <Headline4>
                                {city}
                            </Headline4>
                        </Columns.Col>
                        <Columns.Col narrow>
                            <Text>
                                {country}
                            </Text>
                        </Columns.Col>
                    </Columns>
                </Columns.Col>
                <Columns.Col
                    last
                    narrow
                >
                    <Activity
                        showActivityIf={isUpdating}
                    >
                        <Temp>
                            {temp}
                        </Temp>
                    </Activity>
                </Columns.Col>
            </Columns>
        </Row>
        {(!!weather || !!onUpdate || !!onRemove) && (
            <Row narrow>
                <Columns>
                    <Columns.Col>
                        <Columns>
                            {!!onUpdate && (
                                <Columns.Col narrow>
                                    <Action
                                        icon={<RefreshIcon />}
                                        onClick={onUpdate}
                                    >
                                        Update
                                    </Action>
                                </Columns.Col>
                            )}
                            {!!onRemove && (
                                <Columns.Col narrow>
                                    <Action
                                        icon={<RemoveIcon />}
                                        onClick={onRemove}
                                    >
                                        Remove
                                    </Action>
                                </Columns.Col>
                            )}
                        </Columns>
                    </Columns.Col>
                    <Columns.Col
                        last
                        narrow
                    >
                        <Text>
                            {!isUpdating && weather}
                        </Text>
                    </Columns.Col>
                </Columns>
            </Row>
        )}
    </>
);

Weather.defaultProps = {
    weather: null,
    onUpdate: null,
    onRemove: null,
};

export default React.memo<PropsT>(Weather);
