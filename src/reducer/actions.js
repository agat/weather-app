// @flow

import {
    type CityT,
    type ActionT,
} from '../types';
import {
    type CityWeatherT,
    type CurrentLocationT,
} from './index';


export const ACTION_ADD_CITY = '@city/add';
export const addCity = (city: CityT): ActionT => ({
    type: ACTION_ADD_CITY,
    city: {
        ...city,
        weather: {
            title: '',
            temp: null,
        },
        coordinates: {
            lat: null,
            lon: null,
        },
    },
});

export const ACTION_ADD_CURRENT_CITY = '@city/addCurrent';
export const addCurrentCity = (city: CurrentLocationT): ActionT => ({
    type: ACTION_ADD_CURRENT_CITY,
    city,
});

export const ACTION_UPDATE_CITY_WEATHER = '@city/updateWeather';
export const updateCityWeather = (id: number, weather: CityWeatherT): ActionT => ({
    type: ACTION_UPDATE_CITY_WEATHER,
    id,
    weather,
});

export const ACTION_UPDATE_CITY_COORDINATES = '@city/updateCoordinates';
export const updateCityCoordinates = (id: number, coordinates: CoordinatesT): ActionT => ({
    type: ACTION_UPDATE_CITY_COORDINATES,
    id,
    coordinates,
});

export const ACTION_UPDATE_CURRENT_CITY_WEATHER = '@city/updateCurrentCityWeather';
export const updateCurrentCityWeather = (weather: CityWeatherT): ActionT => ({
    type: ACTION_UPDATE_CURRENT_CITY_WEATHER,
    weather,
});

export const ACTION_REMOVE_CITY = '@city/remove';
export const removeCity = (id: number): ActionT => ({
    type: ACTION_REMOVE_CITY,
    id,
});

export const ACTION_REMOVE_CURRENT_CITY = '@city/removeCurrent';
export const removeCurrentCity = (): ActionT => ({
    type: ACTION_REMOVE_CURRENT_CITY,
});
