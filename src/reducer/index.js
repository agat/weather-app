// @flow

import {
    ACTION_ADD_CITY,
    ACTION_ADD_CURRENT_CITY,
    ACTION_REMOVE_CITY,
    ACTION_REMOVE_CURRENT_CITY,
    ACTION_UPDATE_CITY_WEATHER,
    ACTION_UPDATE_CURRENT_CITY_WEATHER,
    ACTION_UPDATE_CITY_COORDINATES,
} from './actions';

import {
    type CityT,
    type ActionT,
} from '../types';


export type CityWeatherT = {
    title: string,
    temp: ?number
};

export type CoordinatesT = {
    lat: ?number,
    lon: ?number
};

export type CitySavedT = CityT & {
    weather: CityWeatherT,
    coordinates: CoordinatesT
};

export type CurrentLocationT = {
    city: string,
    country: string,
    countryCode: string,
    weather: CityWeatherT,
    coordinates: CoordinatesT
};

export type StateT = {
    cities: CitySavedT[],
    currentLocation: ?CurrentLocationT
};

export const initialState: StateT = {
    cities: [],
    currentLocation: null,
};

export const restoreState = (defaultState) => {
    try {
        let state = window.localStorage.getItem('weather.app');

        if (state) {
            state = JSON.parse(state);
        } else {
            state = defaultState;
        }

        return state;
    } catch (error) {
        return defaultState;
    }
};
export const saveState = (state: StateT) => {
    try {
        window.localStorage.setItem('weather.app', JSON.stringify(state));
    } catch (error) {
        // eslint-disable-next-line no-console
        console.log(error);
    }
};

const reducer = (state: StateT, action: ActionT) => {
    switch (action.type) {
    case ACTION_ADD_CITY: {
        return {
            ...state,
            cities: [
                action.city,
                ...state.cities,
            ],
        };
    }
    case ACTION_ADD_CURRENT_CITY: {
        return {
            ...state,
            currentLocation: action.city,
        };
    }
    case ACTION_REMOVE_CITY: {
        return {
            ...state,
            cities: state.cities.filter(city => city.id !== action.id),
        };
    }
    case ACTION_REMOVE_CURRENT_CITY: {
        return {
            ...state,
            currentLocation: null,
        };
    }
    case ACTION_UPDATE_CITY_WEATHER: {
        return {
            ...state,
            cities: state.cities.map((city) => {
                if (city.id === action.id) {
                    return {
                        ...city,
                        weather: action.weather,
                    };
                }

                return city;
            }),
        };
    }
    case ACTION_UPDATE_CITY_COORDINATES: {
        return {
            ...state,
            cities: state.cities.map((city) => {
                if (city.id === action.id) {
                    return {
                        ...city,
                        coordinates: action.coordinates,
                    };
                }

                return city;
            }),
        };
    }
    case ACTION_UPDATE_CURRENT_CITY_WEATHER: {
        return {
            ...state,
            currentLocation: {
                ...state.currentLocation,
                weather: action.weather,
            },
        };
    }
    default:
        throw new Error();
    }
};

export default reducer;
