// @flow

import {
    type StateT,
} from './reducer';

export type ActionT = {
    type: string
};

export type CityT = {
    id: number,
    name: string,
    state_id: number
};

export type StateT = {
    id: number,
    name: string,
    state_id: number
};

export type WithRouterPropsT = {
    match: Object
};

export type WithDispatchT = {
    dispatch: (ActionT) => void | Promise<any>
};

export type PageT = WithRouterPropsT & WithDispatchT & {
    state: StateT
};
