// @flow

import superagent from 'superagent';

const appid = process.env.REACT_APP_OPEN_WEATHER_KEY;
const url = 'https://api.openweathermap.org/data/2.5/';


export const getWeather = (countryCode: string, cityName: string) => superagent.get(`${url}weather`).query({
    q: `${cityName},${countryCode.toLowerCase()}`,
    appid,
    units: 'metric',
});

export default {
    getWeather,
};
