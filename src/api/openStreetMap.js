// @flow

import superagent from 'superagent';

const url = 'https://nominatim.openstreetmap.org/';


export const reverseSearch = (lat: number, lon: number) => superagent.get(`${url}reverse`).query({
    lat,
    lon,
    zoom: '12',
    addressdetails: '1',
    format: 'jsonv2',
});

export const search = (country: string, city: string) => superagent.get(`${url}search/${city} ${country}`).query({
    limit: 1,
    addressdetails: 1,
    format: 'jsonv2',
});

export default {
    reverseSearch,
};
