// @flow

import {
    updateCityWeather,
    addCurrentCity,
    updateCurrentCityWeather,
    updateCityCoordinates,
} from '../reducer/actions';

import {
    getWeather,
} from '../api/openWeather';
import {
    reverseSearch,
    search,
} from '../api/openStreetMap';

import {
    getCountryByStateId,
} from './country';

import {
    type CityT,
} from '../types';


export const getAndUpdateCityWeather = (city: CityT, dispatch: Function) => {
    const country = getCountryByStateId(city.state_id);

    return getWeather(country.sortname, city.name).then((res) => {
        const {
            main,
            weather,
        } = res.body;

        if (main && weather && weather.length) {
            dispatch(updateCityWeather(city.id, {
                title: weather[0].main,
                temp: main.temp
            }));
        }
    });
};

export const getAndUpdateCityCoordinates = (city: CityT, dispatch: Function) => {
    const country = getCountryByStateId(city.state_id);

    return search(country.name, city.name).then((res) => {
        const result = res.body;

        if (result.length) {
            const {
                lat,
                lon,
            } = result[0];

            if (lat && lon) {
                dispatch(updateCityCoordinates(city.id, {
                    lat,
                    lon,
                }));
            }
        }
    });
};

export const getAndUpdateCurrentCityWeather = (
    countryCode: string,
    city: string,
    dispatch: Function,
) => getWeather(countryCode, city).then((res) => {
    const {
        main,
        weather,
    } = res.body;

    if (main && weather && weather.length) {
        dispatch(updateCurrentCityWeather({
            title: weather[0].main,
            temp: main.temp
        }));
    }
});

export const getAndUpdateCurrentLocation = (dispatch) => {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(async ({ coords }) => {
            const {
                body: {
                    address,
                    lat,
                    lon,
                },
            } = await reverseSearch(coords.latitude, coords.longitude);

            if (address) {
                const {
                    city,
                    country,
                    // eslint-disable-next-line camelcase
                    country_code,
                } = address;

                dispatch(addCurrentCity({
                    city,
                    country,
                    countryCode: country_code,
                    weather: {
                        title: '',
                        temp: null,
                    },
                    coordinates: {
                        lat,
                        lon,
                    },
                }));

                getAndUpdateCurrentCityWeather(country_code, city, dispatch);
            }
        });
    }
};


export default {
    getAndUpdateCityWeather,
};
