import {
    getStateById,
    getCountryById,
} from 'country-state-city';

export const getCountryByStateId = (id: number ) => getCountryById(getStateById(id).country_id);
